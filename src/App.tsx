import React from 'react';
import './App.css';
import {Navigate, Route, Routes} from "react-router-dom";
import Home from "./components/home/Home";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";

function App() {
    return (
        <>
            <Header/>
            <Routes>
                <Route path="home" element={<Home/>}/>
                <Route path="*" element={<Navigate to="home"/>}/>
            </Routes>
            <Footer/>
        </>
    );
}

export default App;
