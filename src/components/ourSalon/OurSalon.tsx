import React from 'react';
import classes from "./ourSalon.module.css";

const OurSalon = () => {
    return (
        <section className={classes.ourSalon}>
            <p>Не знаєте що купити?</p>
            <h4>Відвідай наші салони</h4>
            <p className={classes.lorem}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut duis tortor vitae pellentesque egestas quam pulvinar. Pellentesque porttitor velit sit pellentesque. Suspendisse donec pretium id dignissim. Dignissim ultrices eget orci viverra. Egestas quis et ut ultrices imperdiet lectus nulla tempus. Pharetra lorem sem purus nisi libero viverra ipsum.</p>
            <button>наші салони</button>
        </section>
    );
};

export default OurSalon;