import React, {useState} from 'react';
import classes from './network.module.css'

const Network = () => {
    const [video1, setVideo1] = useState(false)
    const [video2, setVideo2] = useState(false)
    const handleVideo1 = ()=>{
        setVideo1(true)
    }
    const handleVideo2 = ()=>{
        setVideo2(true)
    }
    return (
        <section className={classes.network}>
            <div className={classes.container}>
                <p className={classes.subtitle}>#ojjo_jewerly</p>
                <h4 className={classes.title}>Ми в соціальних мережах</h4>
                <div className={classes.wrapper}>
                    {video1 ? <iframe className={classes.video} width="560" height="315" src="https://www.youtube.com/embed/dQw4w9WgXcQ"
                                      title="YouTube video player" frameBorder="0"
                                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                      allowFullScreen></iframe> :<img onClick={()=>handleVideo1()} className={classes.video} src="https://i.ibb.co/Kz6x1V6/gallery-video-hover.png" alt=""/>}
                    <img src="https://i.ibb.co/bPb9xS8/gallery-image.png" alt=""/>
                    <img src="https://i.ibb.co/h75LcvX/gallery-image-2.png" alt=""/>
                    <img src="https://i.ibb.co/YNmQKpy/gallery-image-1.png" alt=""/>
                    <img src="https://i.ibb.co/3mXHNJG/gallery-image-3.png" alt=""/>
                    {video2 ? <iframe className={classes.video} width="560" height="315" src="https://www.youtube.com/embed/dQw4w9WgXcQ"
                                      title="YouTube video player" frameBorder="0"
                                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                      allowFullScreen></iframe> :<img onClick={()=>handleVideo2()} className={classes.video} src="https://i.ibb.co/Qfj2h5R/gallery-video.png" alt=""/>}
                </div>
            </div>
        </section>
    );
};

export default Network;