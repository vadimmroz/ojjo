import React from 'react';
import classes from './footer.module.css'
import {NavLink} from "react-router-dom";

const Footer = () => {
    return (
        <footer className={classes.footer}>
            <div className={classes.container}>

                <section className={classes.link}>
                    <h5>Корисні посилання</h5>
                    <ul>
                        <li>Доставка</li>
                        <li>Оплата</li>
                        <li>Акції</li>
                        <li>Політика конфіденційності</li>
                    </ul>
                </section>
                <section className={classes.pay}>
                    <h5>Оплата</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper justo, nec,
                        pellentesque.</p>
                    <div>
                        <img src="https://i.ibb.co/v4SBHRB/Rectangle-13.png" alt=""/>
                        <img src="https://i.ibb.co/8cTDGwy/Rectangle-14.png" alt=""/>
                    </div>
                </section>
                <section className={classes.contact}>
                    <h5>Контакти</h5>
                    <ul>
                        <li>8 (812) 234-56-55</li>
                        <li>8 (812) 234-56-55</li>
                        <li>ojjo@ojjo.ru</li>
                    </ul>
                </section>
                <section className={classes.network}>
                    <h5>Соціальні мережі</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper justo, nec,
                        pellentesque.</p>
                    <div>
                        <NavLink to='home'><img src="https://i.ibb.co/4PwkCJz/Vector.png" alt=""/></NavLink>
                        <NavLink to='home'><img src="https://i.ibb.co/CMsZqTc/Vector-1.png" alt=""/></NavLink>
                        <NavLink to='home'> <img src="https://i.ibb.co/RD0TykC/Vector-2.png" alt=""/></NavLink>
                        <NavLink to='home'><img src="https://i.ibb.co/CmV4zcR/Vector-3.png" alt=""/></NavLink>
                        <NavLink to='home'><img src="https://i.ibb.co/j878VhC/Vector-4.png" alt=""/></NavLink>
                    </div>
                </section>

            </div>
            <div className={classes.postScript}>
                <p>(c) 2020 OJJO jewelry</p>
                <NavLink to='home'>Договір публічної офери</NavLink>
                <NavLink to='home'>Контрагентам</NavLink>
                <NavLink to='home'>Зроблено Figma.info</NavLink>
            </div>
        </footer>
    );
};

export default Footer;