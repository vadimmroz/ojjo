import React, {useState} from 'react';
import classes from "./header.module.css"
import {NavLink} from "react-router-dom";
import Menu from "./components/Menu";

const Header = () => {
    const [menu, setMenu] = useState(false)
    const handleMenu = (e: boolean) => {
        setMenu(e)
    }
    // @ts-ignore
    // @ts-ignore
    // @ts-ignore
    return (
        <header className={classes.header}>
            <div className={classes.container}>
                <nav>
                    <NavLink to="home">Контрагентам</NavLink>
                    <NavLink to="home">Дизайнерам</NavLink>
                    <NavLink to="home">Вакансії</NavLink>
                </nav>
                <NavLink to="home"><img src="https://i.ibb.co/xMmnN2t/logo.png" alt="logo"/></NavLink>
                <div className={classes.secondMenu}>
                    <img src="https://i.ibb.co/TTDVX2D/search.png" alt="search"/>
                    <NavLink to="home">Пошук</NavLink>
                    <NavLink to="home">Вхід/Реєстрація</NavLink>
                    <NavLink to="home">
                        <img src="https://i.ibb.co/fkd9BXD/profile.png" alt="profile"/>
                    </NavLink>
                    <NavLink to="home">
                        <img src="https://i.ibb.co/7gXRxLZ/like.png" alt="like"/>
                    </NavLink>
                </div>
                <button onClick={() => handleMenu(true)}>
                    <img src="https://i.ibb.co/fQ9qpHq/Group.png" alt=""/>
                </button>
                {menu && <Menu handleMenu={handleMenu}/>}
            </div>
        </header>
    );
};

export default Header;