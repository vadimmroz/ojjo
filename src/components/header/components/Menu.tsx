import React, {useState} from 'react';
import classes from "./menu.module.css"
import {NavLink} from "react-router-dom";
import classNames from "classnames";

// @ts-ignore
const Menu = ({handleMenu}) => {
    const [finish, setFinish] = useState(false)
    const handleClose = () => {
        setFinish(true)
        setTimeout(() => {
            handleMenu(false)
            setFinish(false)
        }, 500)
    }
    return (
        <div className={classNames(classes.menu, finish && classes.finish)}>
            <div className={classes.header}>
                <NavLink to="home">
                    <img src="https://i.ibb.co/xMmnN2t/logo.png" alt="logo"/>
                </NavLink>
                <button onClick={(handleClose)}>
                    <img src="https://i.ibb.co/FJQB7c3/Vector.png" alt="close"/>
                </button>
            </div>
            <ul>
                <li>
                    <NavLink to="home">Контрагентам</NavLink>
                </li>
                <li>
                    <NavLink to="home">Дизайнерам</NavLink>
                </li>
                <li>
                    <NavLink to="home">Вакансії</NavLink></li>
                <li>
                    <NavLink to="home">Вхід/Реєстрація</NavLink>
            </li>
            </ul>
            <div className={classes.secondMenu}>
                <NavLink to="home">
                    <img src="https://i.ibb.co/Yty1p8p/Vector.png" alt="profile"/>
                </NavLink>
                <NavLink to="home">
                    <img src="https://i.ibb.co/44QYnQJ/Vector.png" alt="like"/>
                </NavLink>
            </div>
        </div>
    );
};

export default Menu;