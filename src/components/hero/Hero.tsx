import React from 'react';
import classes from "./hero.module.css"

const Hero = () => {
    return (
        <section className={classes.home}>
            <p>Довго, дорого, багато!</p>
            <button>каталог изделий </button>
            <div className={classes.line}></div>
            <div className={classes.partner}>
                <img src="https://i.ibb.co/Jyv1fKJ/Rectangle-2.png" alt="partner"/>
                <img src="https://i.ibb.co/Jyv1fKJ/Rectangle-2.png" alt="partner"/>
                <img src="https://i.ibb.co/Jyv1fKJ/Rectangle-2.png" alt="partner"/>
                <img src="https://i.ibb.co/Jyv1fKJ/Rectangle-2.png" alt="partner"/>
                <img src="https://i.ibb.co/Jyv1fKJ/Rectangle-2.png" alt="partner"/>
                <img src="https://i.ibb.co/Jyv1fKJ/Rectangle-2.png" alt="partner"/>
            </div>
        </section>
    );
};

export default Hero;