import React from 'react';
import classes from './exlusive.module.css'

const Exlusive = () => {
    return (
        <section className={classes.exlusive}>
            <div className={classes.container}>
                <p className={classes.subtitle}>Полезные советы и персональный предложения</p>
                <h4 className={classes.title}>Эксклюзивная рассылка</h4>
                <div className={classes.main}>
                    <ul>
                        <li>Персональний менеджер</li>
                        <li>доставка та оформлення</li>
                        <li>Індивідуальний дизайн</li>
                    </ul>
                    <div className={classes.form}>
                        <input type="text" placeholder="ВАШ E-MAIL"/>
                        <button>Відправити</button>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Exlusive;