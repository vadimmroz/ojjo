import React from 'react';
import classes from "./events.module.css"
import {NavLink} from "react-router-dom";
import classNames from "classnames";

const Events = () => {
    return (
        <section className={classNames(classes.events, 'events')}>
            <div className={classes.container}>
                <p>До заходів</p>
                <h3>Справжня краса тут!</h3>
                <nav>
                    <NavLink to="/home">Весілля</NavLink>
                    <NavLink to="/home1">Чоловіку</NavLink>
                    <NavLink to="/home2">Жінці</NavLink>
                    <NavLink to="/home3">Партнеру</NavLink>
                    <NavLink to="/home4">Колекції</NavLink>
                    <NavLink to="/home5">Рідкість</NavLink>
                </nav>
                <div className={classes.wrapper}>

                    <NavLink to='/home'><img src="https://i.ibb.co/VmvpKTD/Rectangle-5.png" alt=""/><p>кільця</p></NavLink>
                    <NavLink to='/home'><img src="https://i.ibb.co/dgPxRqR/Rectangle-5-2.png" alt=""/><p>сережки</p></NavLink>
                    <NavLink to='/home'><img src="https://i.ibb.co/rps2Vhk/Rectangle-5-4.png" alt=""/><p>підвіски</p></NavLink>
                    <NavLink to='/home'><img src="https://i.ibb.co/BwxCCZY/Rectangle-5-1.png" alt=""/><p>запонки</p></NavLink>
                    <NavLink to='/home'><img src="https://i.ibb.co/KWt6QNX/Rectangle-5-3.png" alt=""/><p>браслети</p></NavLink>
                    <NavLink to='/home'><img src="https://i.ibb.co/NFmfbq4/Rectangle-5-5.png" alt=""/><p>годинники</p></NavLink>
                </div>
            </div>
        </section>
    );
};

export default Events;