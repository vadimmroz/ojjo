import React from 'react';
import classes from './articles.module.css'
import {NavLink} from "react-router-dom";

const Articles = () => {
    return (
        <section className={classes.articles}>
            <div className={classes.container}>
                <p className={classes.subtitle}>Корисні статті</p>
                <h4 className={classes.title}>Найкращі поради для вибору дорогих подарунків</h4>
                <div className={classes.slider}>
                    <NavLink to='/home'>
                        <img src="https://i.ibb.co/1GJ793t/Rectangle-5.png" alt=""/>
                        <p>Як обрати годинник для майбутньої жінки</p>
                    </NavLink>
                    <NavLink to='/home'>
                        <img src="https://i.ibb.co/2KR5mYH/Rectangle-5-1.png" alt=""/>
                        <p>Запонки для чоловіка: 7 ключових правил покупки аксесуара</p>
                    </NavLink>
                    <NavLink to='/home'>
                        <img src="https://i.ibb.co/mDfG1Kt/Rectangle-5-2.png" alt=""/>
                        <p>Як вибрати вінчальні кільця</p>
                    </NavLink>
                </div>
                <button>
                    читати наш блог
                </button>
            </div>
        </section>
    );
};

export default Articles;