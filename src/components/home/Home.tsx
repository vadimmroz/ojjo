import React from 'react';
import Hero from "../hero/Hero";
import Events from "../events/Events";
import OurSalon from "../ourSalon/OurSalon";
import Articles from "../articles/Articles";
import Network from "../network/Network";
import Exlusive from "../exlusive/Exlusive";

const Home = () => {
    return (
        <>
            <Hero/>
            <Events/>
            <OurSalon/>
            <Articles/>
            <Network/>
            <Exlusive/>
        </>
    );
};

export default Home;